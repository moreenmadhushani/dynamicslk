import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutSectionComponent } from './about-section/about-section.component';
import { DynamicslkPageComponent } from './dynamicslk-page/dynamicslk-page.component';
import { FooterComponent } from './footer/footer.component';
import { PageSectionComponent } from './page-section/page-section.component';


const routes: Routes = [
  {
    path: '', component: DynamicslkPageComponent
  },
  {
    path: 'footer',
    component: FooterComponent
  }, {
    path: 'about',
    component: AboutSectionComponent
  },
  {
    path: 'sample',
    component: PageSectionComponent
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
