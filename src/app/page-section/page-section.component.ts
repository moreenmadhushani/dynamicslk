import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-section',
  templateUrl: './page-section.component.html',
  styleUrls: ['./page-section.component.css'],
})
export class PageSectionComponent implements OnInit {
  @Input()
  title: string | undefined;

  @Input()
  subHeader: string | undefined;

  @Input()
  description: string | undefined;

  @Input()
  image: string | undefined;

  @Input()
  items: subItem[] | undefined;

  @Input()
  isLeftImage: boolean;

  @Input()
  upperSpace:boolean;

  constructor() {
    this.isLeftImage = true;
    this.upperSpace = false;
  }

  ngOnInit(): void {}
}

export interface subItem {
  title: string;
  description: descriptionItem[];
}

export interface descriptionItem {
  title:string;
  item: string[];
}
